var express = require('express');
var router = express.Router();

let list = [
    {
        name: 'Drunk duck',
        description: 'pub',
        freeTables: 3,
        lat: 49.8407422,
        lng: 24.0342658,
        logo: 'http://lviv-online.com/ua/wp-content/uploads/2017/03/dranken-duch-logo-lviv.jpg',
    },
    {
        name: 'Kupitsa',
        description: 'pub',
        freeTables: 5,
        lat: 49.840214,
        lng: 24.0335334,
        logo: 'http://afishalviv.net/wp-content/uploads/2014/05/Pab-Kupitsa_.jpg',
    },
    {
        name: 'Choven',
        description: 'pub',
        freeTables: 1,
        lat: 49.8431862,
        lng: 24.0321313,
        logo: 'https://media-cdn.tripadvisor.com/media/photo-s/0f/c3/23/81/choven-pub.jpg',
    },
    {
        name: 'Dublin',
        description: 'pub',
        freeTables: 12,
        lat: 49.8429435,
        lng: 24.0281683,
        logo: 'https://s.inyourpocket.com/gallery/21960m.jpg',
    }
]


router.get('/restaurants', function (req, res) {
    res.send(list);
});


module.exports = router;