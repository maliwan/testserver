var express = require('express');
var router = express.Router();

let chart = {
    sex: {
        male: 43,
        female: 24,
        unknown: 4
    },
    budget: {
        low: 23,
        middle: 42,
        hight: 12,
        unknown: 32
    },
    visitors: [
        {
            hour: '10:00',
            visitsCount: 4,
            potensialVisitsCount: 15
        },
        {
            hour: '11:00',
            visitsCount: 2,
            potensialVisitsCount: 12
        },
        {
            hour: '12:00',
            visitsCount: 3,
            potensialVisitsCount: 7
        },
        {
            hour: '13:00',
            visitsCount: 3,
            potensialVisitsCount: 7
        },
        {
            hour: '14:00',
            visitsCount: 3,
            potensialVisitsCount: 8
        },
        {
            hour: '15:00',
            visitsCount: 4,
            potensialVisitsCount: 9
        },
        {
            hour: '16:00',
            visitsCount: 3,
            potensialVisitsCount: 10
        },
        {
            hour: '17:00',
            visitsCount: 3,
            potensialVisitsCount: 12
        },
        {
            hour: '18:00',
            visitsCount: 5,
            potensialVisitsCount: 22
        },
        {
            hour: '19:00',
            visitsCount: 6,
            potensialVisitsCount: 26
        },
        {
            hour: '20:00',
            visitsCount: 6,
            potensialVisitsCount: 27
        },
        {
            hour: '21:00',
            visitsCount: 5,
            potensialVisitsCount: 26
        },
        {
            hour: '22:00',
            visitsCount: 4,
            potensialVisitsCount: 22
        }
    ],
    age: [
        {
            type: 1,
            text: 'under 16',
            count: 3
        },
        {
            type: 2,
            text: '17-25',
            count: 17
        },
        {
            type: 3,
            text: '26-35',
            count: 15
        },
        {
            type: 4,
            text: '36-50',
            count: 11
        },
        {
            type: 5,
            text: '51-65',
            count: 5
        },
        {
            type: 6,
            text: '66+',
            count: 1
        },
        {
            type: 7,
            text: 'unknown',
            count: 7
        }
    ],
    newVsReturning : [
        {
            date: '1 Apr',
            new: 6,
            returning: 26
        },
        {
            date: '1 May',
            new: 2,
            returning: 34
        },
        {
            date: '1 nnu',
            new: 5,
            returning: 21
        },
        {
            date: '1 Jul',
            new: 4,
            returning: 29
        },
        {
            date: '1 Aug',
            new: 7,
            returning: 22
        }
    ],
    repeatedVisits: [
        {
            count: 1,
            visits: 42
        },
        {
            count: 2,
            visits: 35
        },
        {
            count: 3,
            visits: 21
        },
        {
            count: 4,
            visits: 15
        },
        {
            count: 5,
            visits: 7
        },
        {
            count: 6,
            visits: 4
        },
        {
            count: 7,
            visits: 2
        }
    ]
};
let company = {
    info: {
        name: 'Pyanaya utka',
        address: 'Brativ Rohatyntsiv St, 45, Lviv, Lviv Oblast, Ukraine, 79000',
        website: '',
        phone: '+380934524157'
    },
    facilities: {
        booking: false,
        creditCards: true,
        wifi: true,
        placesOnTheStreet: true,
        music: 'none',
        wc: true,
        wheelChairAccess: false,
        lunch: 'no delivery',
    },
    schedule: {
        sunday: {
            day: 'Sunday',
            from: '11:00',
            to: '23:00'
        },
        monday: {
            day: 'Monday',
            from: '11:00',
            to: '23:00'
        },
        tuesday: {
            day: 'Tuesday',
            from: '11:00',
            to: '23:00'
        },
        wednesday: {
            day: 'Wednesday',
            from: '11:00',
            to: '23:00'
        },
        thursday: {
            day: 'Thursday',
            from: '11:00',
            to: '23:00'
        },
        friday: {
            day: 'Friday',
            from: '11:00',
            to: '2:00'
        },
        saturday: {
            day: 'Saturday',
            from: '11:00',
            to: '2:00'
        }
    }
};

router.get('/company/info', function (req, res) {
    res.send(company.info);
});
router.put('/company/info', function (req, res) {
    company.info = req.body;
    res.send(company.info);
});
router.get('/company/facilities', function (req, res) {
    res.send(company.facilities);
});
router.put('/company/facilities', function (req, res) {
    company.facilities = req.body;
    res.send(company.facilities);
});
router.get('/company/schedule', function (req, res) {
    res.send(company.schedule);
});
router.put('/company/schedule', function (req, res) {
    company.schedule = req.body;
    res.send(company.schedule);
});

router.get('/dashboard/charts', function (req, res) {
    res.send(chart);
});

module.exports = router;