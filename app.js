var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwtToken = require('jsonwebtoken');
var auth = require('express-jwt-token');
var cors = require('cors');

var index = require('./routes/index');
var charts = require('./routes/charts');

var public = require('./routes/public');

var app = express();
router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(cors());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
let users = [];

app.use('/', index);

app.post('/token', (req, res) => {
  var username = req.body.username;
  var password = req.body.password;
  var existingUser = users.find((user) => { return user.name === username && user.password === password; });

  if (existingUser) {
    var token = jwtToken.sign({ name: username}, 'secret');
    res.send({ access_token: token });
  }
  else {
    res.status(401).send();
  }
});


app.post('/register', (req, res) => {
  var username = req.body.username;
  var password = req.body.password;

  var existingUser = users.find((user) => { return user.name === username; });
  if (existingUser) {
    res.status(401).send('User already registered');
  }
  else {
    users.push({ name: username, password: password });
    var token = jwtToken.sign({ foo: 'bar' }, 'secret');
    res.send({ access_token: token });
  }
});

router.post('/api/test', (req, res) => {
  res.send({
    data: 'hello'
  });
})
router.post('/api/users', (req, res) => {
  res.send({
    data: users
  });
})

app.use('/', router);
app.use('/public', public);
app.use('/api', auth.jwtAuthProtected, charts);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
